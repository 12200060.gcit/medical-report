# GyelRab

GyelRab is an educational mobile application for  Android and  IOS users that focuses on the history of  Bhutanese leaders. It gives people a broader view of the history of Bhutanese monarchs and their legacy. There is no such digital platform for Bhutanese people  to read the history of Bhutanese great leaders especially in dzongkha. Therefore, for a small country like Bhutan to maintain its own language independence and promote Dzongkha through ICT, it is essential to maintain and promote Dzongkha, which is one of the goals of the Dzongkha Development Committee. 
GyelRab application provides the new platform for the students as well as people of Bhutan to learn the History of Great Leaders and their philosophies for the Country. It is the digital platform for the people of the country to learn about the past and analyze the past events performed by Great leaders sacrificing their life for the country.

Showcase Video link: https://www.youtube.com/watch?v=9eAy0nRFcVk

<img src="https://firebasestorage.googleapis.com/v0/b/my-app-c4b12.appspot.com/o/poster.png?alt=media&token=f9843e2b-4780-4dcf-8232-9f24f75e473d" width = "500" height="800"/>